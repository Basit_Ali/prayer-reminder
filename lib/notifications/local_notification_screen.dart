import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:prayer/notifications/notification_plugin.dart';
import 'package:prayer/screens/home/bottom_navigation.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  int countPendingNotifications;

  List<DateTime> _dailyPrayerTimings = <DateTime>[
    DateTime.now().add(Duration(minutes: 11)),
    DateTime.now().add(Duration(minutes: 12)),
    DateTime.now().add(Duration(minutes: 13)),
    DateTime.now().add(Duration(minutes: 14)),
    DateTime.now().add(Duration(minutes: 15)),
    DateTime.now().add(Duration(minutes: 16)),
  ];

  List _prayerNames = [
    'Fajr',
    'Sunrise',
    'Dhuhr',
    'Asr',
    'Maghrib',
    'Isha',
  ];
  int pendingNotifications;

  @override
  void initState() {
    super.initState();
    notificationPlugin
        .setListenerForLowerVersions(onNotificationInLowerVersions);
    notificationPlugin.setOnNotificationClick(onNotificationClick);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'Notification Screen',
        ),
      ),
      body: Center(
        child: Container(
          child: FlatButton.icon(
              onPressed: () async {
                countPendingNotifications =
                    await notificationPlugin.getPendingNotificationCount();
                // await notificationPlugin.showNotification();
                // await notificationPlugin.scheduleNotification(
                //   2,
                //   'Fajar',
                //   'Fajar Prayer Time is 6:45',
                //   'Payload is Payload',
                //   DateTime.now().add(Duration(minutes: 1)),
                // );
                // await notificationPlugin.repeatNotification();
              },
              icon: Icon(Icons.person),
              label: Text('Show Notifications')),
        ),
      ),
    );
  }

  // @override
  // void initState() {
  //   super.initState();
  //   _createNotifications();
  //   notificationPlugin
  //       .setListenerForLowerVersions(onNotificationInLowerVersions);
  //   notificationPlugin.setOnNotificationClick(onNotificationClick);
  //   print('Fajr Prayer Timing is ${_dailyPrayerTimings[0]}');
  //   Timer(Duration(seconds: 3), () {
  //     Navigator.of(context)
  //         .pushReplacement(MaterialPageRoute(builder: (context) {
  //       return BottomNavBar();
  //     }));
  //   });
  // }

  _createNotifications() async {
    for (int index = 0; index <= 5; index++) {
      if (index != 1) {
        await notificationPlugin.scheduleNotification(
            index,
            _prayerNames[index],
            '${_prayerNames[index]} Prayer Time is ${DateFormat('hh:mm').format(_dailyPrayerTimings[index])}',
            'Payload is Payload',
            _dailyPrayerTimings[index].subtract(Duration(minutes: 10)));
      }
      pendingNotifications =
          await notificationPlugin.getPendingNotificationCount();
      print('Pending Notifications Are $pendingNotifications');
    }
  }

  onNotificationInLowerVersions(ReceivedNotification receivedNotification) {}

  onNotificationClick(String payload) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
      return BottomNavBar();
    }));
  }
}
