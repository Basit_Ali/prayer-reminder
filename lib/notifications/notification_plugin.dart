import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show File, Platform;
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/standalone.dart' as tz;

import 'package:rxdart/rxdart.dart';

class NotificationPlugin {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final BehaviorSubject<ReceivedNotification>
      didReceivedLocalNotificationSubject =
      BehaviorSubject<ReceivedNotification>();
  var initializationSettings;

  NotificationPlugin() {
    init();
  }

  init() async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    // if (Platform.isIOS) {
    //   _requestIosPermissions();
    // }
    initializePlatformSpecifics();
  }

  Future<String> _getLocalTimeZone() async {
    tz.initializeTimeZones();
    String timeZoneName = await FlutterNativeTimezone.getLocalTimezone();
    // return tz.setLocalLocation(tz.getLocation(timeZoneName));
    return timeZoneName;
  }

  Future<tz.Location> _getLocation(String location) async {
    return tz.getLocation(location);
  }

  //Take Permissions From IOS Device For Notifications
  // _requestIosPermissions() {
  //   flutterLocalNotificationsPlugin
  //       .resolvePlatformSpecificImplementation<
  //           IOSFlutterLocalNotificationsPlugin>()
  //       .requestPermissions(
  //         alert: false,
  //         badge: true,
  //         sound: true,
  //       );
  // }

  // initialize Notifications Settings for Android and IOS
  initializePlatformSpecifics() {
    var initializationSettingAndroid =
        AndroidInitializationSettings('prayer_logo');
    var initializationSettingIOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification: (id, title, body, payload) async {
          ReceivedNotification receivedNotification = ReceivedNotification(
              id: id, title: title, body: body, payload: payload);
          didReceivedLocalNotificationSubject.add(receivedNotification);
        });
    initializationSettings = InitializationSettings(
        android: initializationSettingAndroid, iOS: initializationSettingIOS);
  }

  //  LISTEN NOTIFICATIONS ON IOS LESS THEN 10.0
  setListenerForLowerVersions(Function onNotificationInLowerVersions) {
    didReceivedLocalNotificationSubject.listen((receivedNotification) {
      onNotificationInLowerVersions(receivedNotification);
    });
  }

  //  ACTION WHEN USER TAP ON NOTIFICATION
  setOnNotificationClick(Function onNotificationClick) async {
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      onNotificationClick(payload);
    });
  }

  //  Show Notifications
  // Future<void> showNotification() async {
  //   var androidChannelSpecifics = AndroidNotificationDetails(
  //     'showNotifications Id',
  //     'showNotifications Name',
  //     'showNotifications Description',
  //     importance: Importance.max,
  //     priority: Priority.high,
  //     playSound: true,
  //     timeoutAfter: 5000,
  //     sound: RawResourceAndroidNotificationSound('azan_sound'),
  //     styleInformation: DefaultStyleInformation(true, true),
  //   );
  //   var iosChannelSpecifics = IOSNotificationDetails();
  //   var platformChannelSpecifics = NotificationDetails(
  //       android: androidChannelSpecifics, iOS: iosChannelSpecifics);
  //   await flutterLocalNotificationsPlugin.show(
  //       0, 'Test', 'Test body', platformChannelSpecifics,
  //       payload: 'Test Payload');
  // }

//  Schedule Pre Notification
  Future<void> scheduleNotification(
    int id,
    String title,
    String body,
    String payload,
    DateTime time,
  ) async {
    String timeZoneName = await _getLocalTimeZone();
    var location = await _getLocation(timeZoneName);
    SharedPreferences _pref = await SharedPreferences.getInstance();
    bool preNotificationActivation = _pref.getBool('preAlarmState') ?? true;
    var androidChannelSpecifics = AndroidNotificationDetails(
      'preNotifications_Id $preNotificationActivation',
      'preNotifications_Name $preNotificationActivation',
      'preNotifications_Description $preNotificationActivation',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
      playSound: preNotificationActivation,
      // timeoutAfter: preNotificationActivation ? 20000 : 5000,
      sound: RawResourceAndroidNotificationSound(
          preNotificationActivation ? 'azan_sound' : null),
      largeIcon: DrawableResourceAndroidBitmap('prayer_logo'),
      enableLights: true,
      color: Color.fromARGB(255,28, 87, 155),
      ledColor: Color.fromARGB(255,28, 87, 155),
      ledOffMs: 500,
      ledOnMs: 1000,
      styleInformation: DefaultStyleInformation(true, true),
    );
    var iosChannelSpecifics = IOSNotificationDetails(
        sound: 'azan_sound.wav',
        );
    var platformChannelSpecifics = NotificationDetails(
        android: androidChannelSpecifics, iOS: iosChannelSpecifics);
    await flutterLocalNotificationsPlugin.zonedSchedule(
      id,
      title,
      body,
      tz.TZDateTime.from(time, location),
      platformChannelSpecifics,
      payload: payload,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  //  Schedule Post Notification
  Future<void> schedulePostNotification(
    int id,
    String title,
    String body,
    String payload,
    DateTime time,
  ) async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    bool postNotificationActivation = _pref.getBool('postAlarmState') ?? true;
    String timeZoneName = await _getLocalTimeZone();
    var location = await _getLocation(timeZoneName);
    var androidChannelSpecifics = AndroidNotificationDetails(
      'postNotifications_Id $postNotificationActivation',
      'postNotifications_Name $postNotificationActivation',
      'postNotifications_Description $postNotificationActivation',
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
      playSound: postNotificationActivation,
      // timeoutAfter: postNotificationActivation ? 20000 : 5000,
      sound: RawResourceAndroidNotificationSound(
          postNotificationActivation ? 'azan_sound' : null),
      largeIcon: DrawableResourceAndroidBitmap('prayer_logo'),
      enableLights: true,
      color: Color.fromARGB(255,28, 87, 155),
      ledColor: Color.fromARGB(255,28, 87, 155),
      ledOffMs: 500,
      ledOnMs: 1000,
      styleInformation: DefaultStyleInformation(true, true),
    );
    var iosChannelSpecifics = IOSNotificationDetails(
      sound: 'azan_sound.wav',
    );
    var platformChannelSpecifics = NotificationDetails(
        android: androidChannelSpecifics, iOS: iosChannelSpecifics);
    await flutterLocalNotificationsPlugin.zonedSchedule(
      id,
      title,
      body,
      tz.TZDateTime.from(time, location),
      platformChannelSpecifics,
      payload: payload,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

//  Repeat Notifications
//   Future<void> repeatNotification() async {
//     var androidChannelSpecifics = AndroidNotificationDetails(
//       'channel Id 2',
//       'channel Name 2',
//       'channel Description 2',
//       importance: Importance.high,
//       priority: Priority.high,
//       // playSound: true,
//       // timeoutAfter: 5000,
//       // styleInformation: DefaultStyleInformation(true, true),
//     );
//     var iosChannelSpecifics = IOSNotificationDetails();
//     var platformChannelSpecifics = NotificationDetails(
//         android: androidChannelSpecifics, iOS: iosChannelSpecifics);
//     await flutterLocalNotificationsPlugin.periodicallyShow(0, 'Test',
//         'Test body', RepeatInterval.everyMinute, platformChannelSpecifics,
//         payload: 'Test Payload');
//   }

//  Get Pending Notifications Count
  Future<int> getPendingNotificationCount() async {
    List<PendingNotificationRequest> p =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return p.length;
  }

//Cancel Future Notifications using ID
//   Future<void> cancelNotificationUsingId() async {
//     await flutterLocalNotificationsPlugin.cancel(0);
//   }

//Cancel All Future Notifications For Specific User
  Future<void> cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }
}

NotificationPlugin notificationPlugin = NotificationPlugin();

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification(
      {@required this.id,
      @required this.title,
      @required this.body,
      @required this.payload});
}
