import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:prayer/notifications/notification_plugin.dart';
import 'package:prayer/screens/home/bottom_navigation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show File, Platform;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  int pendingNotifications;
  int preNotificationDelay = 5;
  int postNotificationDelay = 4;
  static bool checkIosPermissions = false;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  List<DateTime> _dailyPrayerTimings = <DateTime>[
    DateTime.now().add(Duration(minutes: 6)),
    DateTime.now().add(Duration(minutes: 8)),
    DateTime.now().add(Duration(minutes: 10)),
    DateTime.now().add(Duration(minutes: 12)),
    DateTime.now().add(Duration(minutes: 14)),
    DateTime.now().add(Duration(minutes: 16)),
  ];

  List _prayerNames = [
    'Fajr',
    'Sunrise',
    'Dhuhr',
    'Asr',
    'Maghrib',
    'Isha',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Image.asset('assets/images/splash_screen.jpg'),
        ),
      ),
    );
  }

  Future<void> _setCounter() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    setState(() {
      preNotificationDelay = _pref.getInt('preAlarmTimer') ?? 5;
    });
    _createPreNotifications();
    // notificationPlugin
    //     .setListenerForLowerVersions(onNotificationInLowerVersions);
    _setPostNotifications();
  }

  _setPostNotifications() {
    _createPostNotifications();
  }

  //Take Permissions From IOS Device For Notifications
  _requestIosPermissions() async {
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        .requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        )
        .then((value) {
      if (value) {
        _setCounter();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    if (Platform.isIOS) {
      _requestIosPermissions();
    } else {
      _setCounter();
    }
    notificationPlugin.setOnNotificationClick(onNotificationClick);
    notificationPlugin
        .setListenerForLowerVersions(onNotificationInLowerVersions);
    Timer(Duration(seconds: 3), () {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return BottomNavBar();
        // return NotificationScreen();
      }));
    });
  }

  _createPreNotifications() async {
    for (int index = 0; index <= 5; index++) {
      if (index != 1) {
        int checkTime = _dailyPrayerTimings[index]
            .subtract(Duration(minutes: preNotificationDelay))
            .difference(DateTime.now())
            .inSeconds;
        if (checkTime < 5) continue;
        await notificationPlugin.scheduleNotification(
            index,
            _prayerNames[index],
            '${_prayerNames[index]} Prayer Time is ${DateFormat('hh:mm').format(_dailyPrayerTimings[index])}',
            'Payload is Payload',
            _dailyPrayerTimings[index]
                .subtract(Duration(minutes: preNotificationDelay)));
        print(
            'Pre Notifications is ${_dailyPrayerTimings[index].subtract(Duration(minutes: preNotificationDelay))}');
      }
    }
  }

  _createPostNotifications() async {
    for (int i = 0; i <= 5; i++) {
      if (i != 1) {
        int checkTime = _dailyPrayerTimings[i]
            .subtract(Duration(minutes: postNotificationDelay))
            .difference(DateTime.now())
            .inSeconds;
        if (checkTime < 5) continue;
        await notificationPlugin.schedulePostNotification(
            i + 6,
            _prayerNames[i],
            '${_prayerNames[i]} Prayer Time Was ${DateFormat('hh:mm').format(_dailyPrayerTimings[i])}',
            'Payload is Payload',
            _dailyPrayerTimings[i]
                .subtract(Duration(minutes: postNotificationDelay)));
        print(
            'Post Notification Set to ${_dailyPrayerTimings[i].subtract(Duration(minutes: postNotificationDelay))}');
      }
      pendingNotifications =
          await notificationPlugin.getPendingNotificationCount();
      print('Total Pending Notifications Are $pendingNotifications');
    }
  }

  onNotificationInLowerVersions(ReceivedNotification receivedNotification) {}

  onNotificationClick(String payload) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
      return BottomNavBar();
    }));
  }
}
