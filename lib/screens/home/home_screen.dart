import 'package:flutter/material.dart';
import 'package:prayer/screens/home/day_and_date.dart';
import 'package:prayer/screens/home/prayer_timing.dart';

class HomeScreen extends StatefulWidget {
  final DateTime calendarTapDate;
  HomeScreen([this.calendarTapDate]);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
   return Scaffold(
     backgroundColor: Color(0xff2372AD),
     appBar: AppBar(
       backgroundColor: Colors.transparent,
       elevation: 0.0,
     ),
     body: PrayersTiming(widget.calendarTapDate),
   );
  }


}
