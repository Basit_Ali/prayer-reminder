import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:prayer/screens/home/prayer_timing_list.dart';
import 'package:prayer/shared/size_config.dart';

import 'day_and_date.dart';

class PrayersTiming extends StatefulWidget {
  final DateTime calendarTapDate;

  PrayersTiming([this.calendarTapDate]);

  @override
  _PrayersTimingState createState() => _PrayersTimingState();
}

class _PrayersTimingState extends State<PrayersTiming> {
  DateTime toDay;
  String dayFromToDay;
  int _currentSliderPos = 0;
  DateTime dayAndDateTime;
  var builderDaysCheck;
  var itemCount;
  List _daysList = [
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  CarouselController carouselController = CarouselController();

  @override
  void initState() {
    super.initState();
    toDay = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.calendarTapDate == null) {
      builderDaysCheck = DateTime.utc(toDay.year + 1, DateTime.january, 1)
          .difference(DateTime(toDay.year, toDay.month, toDay.day))
          .inDays;
    } else {
      builderDaysCheck = DateTime.utc(toDay.year + 1, DateTime.january, 1)
          .difference(DateTime(widget.calendarTapDate.year,
              widget.calendarTapDate.month, widget.calendarTapDate.day))
          .inDays;
    }
    if (builderDaysCheck >= 7) {
      itemCount = 7;
    } else {
      itemCount = builderDaysCheck;
    }
    SizeConfig().init(context);
    double height1 = SizeConfig.safeBlockVertical * 100;
    return CarouselSlider.builder(
      options: CarouselOptions(
          height: height1 * 1.0,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          viewportFraction: 1.0,
          autoPlay: false,
          onPageChanged: (index, reason) {
            setState(() {
              _currentSliderPos = index;
            });
          }),
      carouselController: carouselController,
      itemCount: itemCount,
      itemBuilder: (context, i) {
        if (widget.calendarTapDate == null) {
          dayFromToDay =
              DateFormat('EEEE').format(toDay.add(Duration(days: i)));
          dayAndDateTime = toDay.add(Duration(days: i));
        } else {
          dayFromToDay = DateFormat('EEEE')
              .format(widget.calendarTapDate.add(Duration(days: i)));
          dayAndDateTime = widget.calendarTapDate.add(Duration(days: i));
        }
        return Column(
          children: [
            Expanded(flex: 2, child: DayAndDate(dayAndDateTime)),
            Expanded(
              flex: 6,
              child: Container(
                decoration: BoxDecoration(
                    color: Color(0xffD2E7F8),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(
                            6.54 * SizeConfig.safeBlockVertical),
                        topLeft: Radius.circular(
                            6.54 * SizeConfig.safeBlockVertical))),
                padding: EdgeInsets.only(
                    left: 3.2 * SizeConfig.safeBlockVertical,
                    right: 3.2 * SizeConfig.safeBlockVertical,
                    top: 3.2 * SizeConfig.safeBlockVertical),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      dayFromToDay,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 3.9 * SizeConfig.safeBlockVertical,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 6.0,
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Athan',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 3.5 * SizeConfig.safeBlockVertical,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    PrayerTimingList(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: _daysList.map((day) {
                        int index = _daysList.indexOf(day);
                        if (index + 1 <= itemCount) {
                          return InkWell(
                            child: Container(
                              width: 1.75 * SizeConfig.safeBlockVertical,
                              height: 1.75 * SizeConfig.safeBlockVertical,
                              margin: EdgeInsets.symmetric(
                                  vertical: 1.3 * SizeConfig.safeBlockVertical,
                                  horizontal:
                                      0.4 * SizeConfig.safeBlockVertical),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _currentSliderPos == index
                                    ? Color.fromRGBO(0, 0, 0, 0.9)
                                    : Color.fromRGBO(0, 0, 0, 0.4),
                              ),
                            ),
                            onTap: () {
                              carouselController.jumpToPage(index);
                              _currentSliderPos = index;
                            },
                          );
                        }
                        else{
                          return SizedBox();
                        }
                      }).toList(),
                    ),
                    SizedBox(
                      height: 4 * SizeConfig.safeBlockVertical,
                    )
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
