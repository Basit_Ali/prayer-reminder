import 'package:flutter/material.dart';
import 'package:prayer/screens/calendar/calendar.dart';
import 'package:prayer/screens/home/home_screen.dart';
import 'package:prayer/screens/more_setting/settings.dart';
import 'package:prayer/shared/size_config.dart';

class BottomNavBar extends StatefulWidget {
  DateTime calendarTapDate;

  BottomNavBar([this.calendarTapDate]);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  var _selectedIndex = 0;
  List<Widget> _widgetOption = <Widget>[
    HomeScreen(),
    Calendar(),
    Settings(),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: _setCurrentScreen(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 1 * SizeConfig.safeBlockVertical),
              child: Icon(
                Icons.person,
                size: 4.37 * SizeConfig.safeBlockVertical,
              ),
            ),
            label: 'Prayer',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 1 * SizeConfig.safeBlockVertical),
              child: Icon(
                Icons.calendar_today_outlined,
                size: 4.37 * SizeConfig.safeBlockVertical,
              ),
            ),
            label: 'Calendar',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 1 * SizeConfig.safeBlockVertical),
              child: Icon(
                Icons.more_horiz_outlined,
                size: 4.37 * SizeConfig.safeBlockVertical,
              ),
            ),
            label: 'More',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xff2372AD),
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    if (_selectedIndex != index) {
      setState(() {
        _selectedIndex = index;
        widget.calendarTapDate = null;
      });
    }
  }

  Widget _setCurrentScreen() {
    Widget currentScreen;
    if (_selectedIndex == 0) {
      currentScreen = HomeScreen(widget.calendarTapDate);
    } else if (_selectedIndex == 1) {
      currentScreen = _widgetOption.elementAt(_selectedIndex);
    } else if (_selectedIndex == 2) {
      currentScreen = _widgetOption.elementAt(_selectedIndex);
    }
    return currentScreen;
  }
}
