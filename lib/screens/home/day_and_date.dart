import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:intl/intl.dart';
import 'package:prayer/shared/size_config.dart';

class DayAndDate extends StatefulWidget {
  final DateTime tapDate;

  DayAndDate([this.tapDate]);

  @override
  _DayAndDateState createState() => _DayAndDateState();
}

class _DayAndDateState extends State<DayAndDate> {
  String _regularDateFormatted;
  String _hijriDateFormatted;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    _regularDateFormatted =
        DateFormat('EEE, d MMMM yyyy').format(widget.tapDate);
    var _setHijriTapDate = HijriCalendar.fromDate(widget.tapDate);
    _hijriDateFormatted = _setHijriTapDate.toFormat('d MMMM yyyy');
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 1 * SizeConfig.safeBlockVertical),
          child: Text(
            _regularDateFormatted,
            style: TextStyle(
                fontSize: 4.81 * SizeConfig.safeBlockVertical,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ),
        Text(
          _hijriDateFormatted,
          style: TextStyle(
              fontSize: 3.93 * SizeConfig.safeBlockVertical,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
      ],
    );
  }
}
