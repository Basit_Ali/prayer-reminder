import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:prayer/shared/size_config.dart';

class PrayerTimingList extends StatefulWidget {
  @override
  _PrayerTimingListState createState() => _PrayerTimingListState();
}

class _PrayerTimingListState extends State<PrayerTimingList> {
  List _prayerNames = [
    'Fajr',
    'Sunrise',
    'Dhuhr',
    'Asr',
    'Maghrib',
    'Isha',
  ];

  List<DateTime> _dailyPrayerTimings = <DateTime>[
    DateTime.now().add(Duration(minutes: 6)),
    DateTime.now().add(Duration(minutes: 8)),
    DateTime.now().add(Duration(minutes: 10)),
    DateTime.now().add(Duration(minutes: 12)),
    DateTime.now().add(Duration(minutes: 14)),
    DateTime.now().add(Duration(minutes: 16)),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Expanded(
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: _prayerNames.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: EdgeInsets.only(
                  top: 1.3 * SizeConfig.safeBlockVertical,
                  bottom: 1.3 * SizeConfig.safeBlockVertical),
              child: Row(
                children: [
                  Icon(
                    Icons.wb_sunny_outlined,
                    size: 4.37 * SizeConfig.safeBlockVertical,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 1 * SizeConfig.safeBlockVertical),
                    child: Text(
                      _prayerNames[index],
                      style: TextStyle(
                        fontSize: 3.5 * SizeConfig.safeBlockVertical,
                      ),
                    ),
                  ),
                  Spacer(),
                  Text(
                    DateFormat('hh:mm').format(_dailyPrayerTimings[index]),
                    style: TextStyle(
                      fontSize: 3.5 * SizeConfig.safeBlockVertical,
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 1 * SizeConfig.safeBlockVertical),
                    child: Icon(
                      Icons.notifications_none_outlined,
                      color: Colors.black54,
                      size: 4.37 * SizeConfig.safeBlockVertical,
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
