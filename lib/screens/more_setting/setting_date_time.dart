import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:intl/intl.dart';
import 'package:prayer/shared/size_config.dart';

class SettingDateTime extends StatefulWidget {
  @override
  _SettingDateTimeState createState() => _SettingDateTimeState();
}

class _SettingDateTimeState extends State<SettingDateTime> {
  DateTime regularDate;
  var hijriDate;
  String _regularDateFormatted;
  String _hijriDateFormatted;

  @override
  void initState() {
    super.initState();
    regularDate = DateTime.now();
    hijriDate = HijriCalendar.now();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    _regularDateFormatted = DateFormat('EEE, d MMMM yyyy').format(regularDate);
    _hijriDateFormatted = hijriDate.toFormat('d MMMM yyyy');
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 1 * SizeConfig.safeBlockVertical),
          child: Text(
            _regularDateFormatted,
            style: TextStyle(
                fontSize: 4.81 * SizeConfig.safeBlockVertical,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ),
        Text(
          _hijriDateFormatted,
          style: TextStyle(
              fontSize: 3.93 * SizeConfig.safeBlockVertical,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
      ],
    );
  }
}
