import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:prayer/notifications/notification_plugin.dart';
import 'package:prayer/screens/more_setting/setting_date_time.dart';
import 'package:prayer/shared/size_config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show File, Platform;

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  //Set Notifications
  int pendingNotifications;
  int preNotificationDelay = 5;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  List<DateTime> _dailyPrayerTimings = <DateTime>[
    DateTime.now().add(Duration(minutes: 6)),
    DateTime.now().add(Duration(minutes: 7)),
    DateTime.now().add(Duration(minutes: 8)),
    DateTime.now().add(Duration(minutes: 9)),
    DateTime.now().add(Duration(minutes: 11)),
    DateTime.now().add(Duration(minutes: 12)),
  ];

  List _prayerNames = [
    'Fajr',
    'Sunrise',
    'Dhuhr',
    'Asr',
    'Maghrib',
    'Isha',
  ];

  //End Set Notifications

  int _minTime = 5;
  int _maxTime = 15;
  int counter;
  int _postNotificationAlarm = 5;
  bool preNotificationsActive = true;
  bool postNotificationsActive = true;
  var status;

  getIntToSf() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int startUpNumber = await prefs.get('preAlarmTimer') ?? 5;
    return startUpNumber;
  }

  getBoolPreAlarm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool preAlarmState = await prefs.get('preAlarmState') ?? true;
    return preAlarmState;
  }

  getBoolPostAlarm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool postAlarmState = await prefs.get('postAlarmState') ?? true;
    return postAlarmState;
  }

  togglePreAlarmState(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool preAlarmState = await getBoolPreAlarm();
    await _prefs.setBool('preAlarmState', !preAlarmState);
    setState(() {
      preNotificationsActive = !preAlarmState;
    });
    NotificationPlugin().cancelAllNotifications();
    _setNotifications(context);
  }

  togglePostAlarmState(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool postAlarmState = await getBoolPostAlarm();
    await _prefs.setBool('postAlarmState', !postAlarmState);
    setState(() {
      postNotificationsActive = !postAlarmState;
    });
    NotificationPlugin().cancelAllNotifications();
    _setNotifications(context);
  }

  _incrementPreAlarmTime(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int incrementNumber = await getIntToSf();
    if (incrementNumber < _maxTime) {
      incrementNumber += 5;
      NotificationPlugin().cancelAllNotifications();
      await prefs.setInt('preAlarmTimer', incrementNumber);
      setState(() {
        counter = incrementNumber;
      });
      _setNotifications(context);
    }
  }

  _decrementPreAlarmTime(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int decrementNumber = await getIntToSf();
    await prefs.setInt('preAlarmTimer', decrementNumber);
    if (decrementNumber > _minTime) {
      decrementNumber -= 5;
      NotificationPlugin().cancelAllNotifications();
      await prefs.setInt('preAlarmTimer', decrementNumber);
      setState(() {
        counter = decrementNumber;
      });
      _setNotifications(context);
    }
  }

  setSharedPrefs() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      counter = _prefs.getInt('preAlarmTimer') ?? 5;
      preNotificationsActive = _prefs.getBool('preAlarmState') ?? true;
      postNotificationsActive = _prefs.getBool('postAlarmState') ?? true;
    });
  }

  @override
  void initState() {
    super.initState();
    setSharedPrefs();
  }

  //Take Permissions From IOS Device For Notifications
  // _requestIosPermissions() async {
  //
  //   status = await Permission.notification.status;
  //   if (status.isGranted) {
  //     _createNotifications();
  //     _createPostNotifications();
  //   }
  // }

  _setNotifications(BuildContext context) async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    status = await Permission.notification.status;
    if (Platform.isIOS && status.isGranted) {
      print('Permissions were Granted!');
      _createNotifications();
      _createPostNotifications();
    } else if (Platform.isIOS && status.isDenied) {
      print('Permissions were Denied');
      _showToast(context);
    } else if(Platform.isAndroid){
      _createNotifications();
      _createPostNotifications();
    }
  }

  _createNotifications() async {
    for (int index = 0; index <= 5; index++) {
      if (index != 1) {
        int checkTime = _dailyPrayerTimings[index]
            .subtract(Duration(minutes: counter))
            .difference(DateTime.now())
            .inSeconds;
        if (checkTime < 5) continue;
        await notificationPlugin.scheduleNotification(
            index,
            _prayerNames[index],
            '${_prayerNames[index]} Prayer Time is ${DateFormat('hh:mm').format(_dailyPrayerTimings[index])}',
            'Payload is Payload',
            _dailyPrayerTimings[index].subtract(Duration(minutes: counter)));
        print(
            'Pre Notifications is ${_dailyPrayerTimings[index].subtract(Duration(minutes: counter))}');
      }
    }
  }

  _createPostNotifications() async {
    for (int i = 0; i <= 5; i++) {
      if (i != 1) {
        int checkTime = _dailyPrayerTimings[i]
            .subtract(Duration(minutes: _postNotificationAlarm - 1))
            .difference(DateTime.now())
            .inSeconds;
        if (checkTime < 5) continue;
        await notificationPlugin.schedulePostNotification(
            i + 6,
            _prayerNames[i],
            '${_prayerNames[i]} Prayer Time Was ${DateFormat('hh:mm').format(_dailyPrayerTimings[i])}',
            'Payload is Payload',
            _dailyPrayerTimings[i]
                .subtract(Duration(minutes: _postNotificationAlarm - 1)));
        print(
            'Post Notification Set to ${_dailyPrayerTimings[i].subtract(Duration(minutes: _postNotificationAlarm - 1))}');
      }
    }
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Change Permissions for Notifications'),
        action: SnackBarAction(
            label: 'Settings',
            onPressed: () {
              openAppSettings();
            }),
      ),
    );
  }

  // onNotificationInLowerVersions(ReceivedNotification receivedNotification) {}

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Color(0xff2372AD),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        title: Text(
          'Settings',
          style: TextStyle(
            color: Colors.white,
            fontSize: 4.7 * SizeConfig.safeBlockVertical,
          ),
        ),
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Expanded(flex: 1, child: SettingDateTime()),
          Expanded(
            flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  color: Color(0xffD2E7F8),
                  borderRadius: BorderRadius.only(
                      topRight:
                          Radius.circular(6.54 * SizeConfig.safeBlockVertical),
                      topLeft: Radius.circular(
                          6.54 * SizeConfig.safeBlockVertical))),
              padding: EdgeInsets.only(
                  left: 2.1 * SizeConfig.safeBlockVertical,
                  right: 1.1 * SizeConfig.safeBlockVertical,
                  top: 3.2 * SizeConfig.safeBlockVertical),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'Pre Notifications',
                        style: TextStyle(
                          fontSize: 2.9 * SizeConfig.safeBlockVertical,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                              icon: Icon(Icons.remove),
                              padding: EdgeInsets.all(0.0),
                              iconSize: 2.9 * SizeConfig.safeBlockVertical,
                              onPressed: () async {
                                await _decrementPreAlarmTime(context);
                              }),
                          Container(
                            padding: EdgeInsets.all(0.0),
                            // width: 9.5 * SizeConfig.safeBlockVertical,
                            child: Text(
                              counter == null
                                  ? ''
                                  : '${counter.toString()} min',
                              style: TextStyle(
                                fontSize: 2.9 * SizeConfig.safeBlockVertical,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.add),
                            padding: EdgeInsets.all(0.0),
                            iconSize: 2.9 * SizeConfig.safeBlockVertical,
                            onPressed: () async {
                              await _incrementPreAlarmTime(context);
                            },
                          ),
                        ],
                      ),
                      IconButton(
                        icon: Icon(preNotificationsActive
                            ? Icons.notifications_none_outlined
                            : Icons.notifications_off_outlined),
                        iconSize: 4.3 * SizeConfig.safeBlockVertical,
                        onPressed: () async {
                          await togglePreAlarmState(context);
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.4 * SizeConfig.safeBlockVertical,
                  ),
                  Row(
                    children: [
                      Text(
                        'Post Notifications',
                        style: TextStyle(
                          fontSize: 2.9 * SizeConfig.safeBlockVertical,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                      IconButton(
                          icon: Icon(
                            Icons.remove,
                            color: Colors.transparent,
                          ),
                          padding: EdgeInsets.all(0.0),
                          iconSize: 0.1 * SizeConfig.safeBlockVertical,
                          onPressed: () {}),
                      Container(
                        padding: EdgeInsets.all(0.0),
                        width: 8.5 * SizeConfig.safeBlockVertical,
                        child: Text(
                          '${_postNotificationAlarm.toString()} min',
                          style: TextStyle(
                            fontSize: 2.9 * SizeConfig.safeBlockVertical,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.add,
                            color: Colors.transparent,
                          ),
                          padding: EdgeInsets.all(0.0),
                          iconSize: 0.1 * SizeConfig.safeBlockVertical,
                          onPressed: () {}),
                      IconButton(
                        icon: Icon(postNotificationsActive
                            ? Icons.notifications_none_outlined
                            : Icons.notifications_off_outlined),
                        iconSize: 4.3 * SizeConfig.safeBlockVertical,
                        onPressed: () async {
                          await togglePostAlarmState(context);
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
