import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:intl/intl.dart';
import 'package:prayer/screens/home/bottom_navigation.dart';
import 'package:prayer/shared/size_config.dart';
import 'package:table_calendar/table_calendar.dart';

class ShowCalendar extends StatefulWidget {
  @override
  _ShowCalendarState createState() => _ShowCalendarState();
}

class _ShowCalendarState extends State<ShowCalendar> {
  List _monthsList = [
    '',
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  int _currentSliderPos;
  CalendarController _calendarController;
  var hijriDate;
  String formatHijriDate;
  DateTime checkDate;
  DateTime monthFirstDate;
  DateTime monthLastDate;
  var hijriFirstDayMonth;
  var hijriLastDayMonth;
  String firstDateHijriMonth;
  String lastDateHijriMonth;

  @override
  void initState() {
    super.initState();
    hijriDate = HijriCalendar.now();
    _calendarController = CalendarController();
    checkDate = DateTime.now();
    monthFirstDate = DateTime(checkDate.year, checkDate.month, 1);
    monthLastDate = DateTime(checkDate.year, checkDate.month + 1, 0);
    hijriFirstDayMonth = HijriCalendar.fromDate(monthFirstDate);
    hijriLastDayMonth = HijriCalendar.fromDate(monthLastDate);
    firstDateHijriMonth = hijriFirstDayMonth.toFormat('MMMM');
    lastDateHijriMonth = hijriLastDayMonth.toFormat('MMMM');
    _currentSliderPos = checkDate.month;
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      if (first.day != 1) {
        monthFirstDate = DateTime(first.year, first.month + 1, 1);
        _currentSliderPos = monthFirstDate.month;
      } else {
        monthFirstDate = first;
        _currentSliderPos = monthFirstDate.month;
      }
      if (last.day <= 31 && last.day >= 28) {
        monthLastDate = last;
      } else {
        monthLastDate = DateTime(last.year, last.month, 0);
      }
      hijriFirstDayMonth = HijriCalendar.fromDate(monthFirstDate);
      hijriLastDayMonth = HijriCalendar.fromDate(monthLastDate);
      firstDateHijriMonth = hijriFirstDayMonth.toFormat('MMMM');
      lastDateHijriMonth = hijriLastDayMonth.toFormat('MMMM');
    });
  }

  void _onDaySelected(
    DateTime date,
    _,
    events,
  ) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
      return BottomNavBar(date);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: 2 * SizeConfig.safeBlockVertical),
      child: Column(
        children: [
          Expanded(
            flex: 15,
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(top: 3 * SizeConfig.safeBlockVertical),
                  child: Container(
                    child: Text(
                      '$firstDateHijriMonth / $lastDateHijriMonth',
                      style: TextStyle(
                        fontSize: 3.5 * SizeConfig.safeBlockVertical,
                        color: Colors.redAccent,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                  ),
                ),
                TableCalendar(
                  rowHeight: 11 * SizeConfig.safeBlockVertical,
                  calendarController: _calendarController,
                  availableGestures: AvailableGestures.horizontalSwipe,
                  startingDayOfWeek: StartingDayOfWeek.monday,
                  daysOfWeekStyle: DaysOfWeekStyle(
                    weekendStyle: TextStyle(
                      fontSize: 3.5 * SizeConfig.safeBlockVertical,
                      color: Colors.black54,
                    ),
                    weekdayStyle: TextStyle(
                      fontSize: 3.5 * SizeConfig.safeBlockVertical,
                      color: Colors.black54,
                    ),
                  ),
                  startDay: DateTime.utc(checkDate.year, DateTime.january, 1),
                  endDay: DateTime.utc(checkDate.year, DateTime.december, 31),
                  headerStyle: HeaderStyle(
                    leftChevronVisible: false,
                    rightChevronVisible: false,
                    formatButtonVisible: false,
                    centerHeaderTitle: true,
                    formatButtonShowsNext: false,
                    headerMargin: EdgeInsets.only(
                        bottom: 1.4 * SizeConfig.safeBlockVertical),
                    titleTextBuilder: (date, locale) =>
                        DateFormat('MMMM - yyyy').format(date),
                    titleTextStyle: TextStyle(
                      fontSize: 3.93 * SizeConfig.safeBlockVertical,
                    ),
                  ),
                  calendarStyle: CalendarStyle(
                    contentPadding: EdgeInsets.all(0.0),
                  ),

                  //  Start Builders
                  builders: CalendarBuilders(
                    //  All Days Builder
                    dayBuilder: (context, date, _) => Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 1 * SizeConfig.safeBlockVertical),
                      child: Column(
                        children: [
                          Expanded(
                            child: Text(
                              date.day.toString(),
                              style: TextStyle(
                                // color: Colors.black,
                                fontSize: 3.5 * SizeConfig.safeBlockVertical,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              HijriCalendar.fromDate(date).hDay.toString(),
                              style: TextStyle(
                                color: Colors.redAccent,
                                fontSize: 3.5 * SizeConfig.safeBlockVertical,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    //  Today Builder
                    todayDayBuilder: (context, date, events) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 0.5 * SizeConfig.safeBlockVertical),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.all(0.0),
                                alignment: Alignment.center,
                                height: 7.87 * SizeConfig.safeBlockVertical,
                                width: 7.87 * SizeConfig.safeBlockVertical,
                                decoration: BoxDecoration(
                                  color: Colors.redAccent,
                                  shape: BoxShape.circle,
                                ),
                                child: Text(
                                  date.day.toString(),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize:
                                        3.5 * SizeConfig.safeBlockVertical,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                HijriCalendar.fromDate(date).hDay.toString(),
                                style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize:
                                        3.5 * SizeConfig.safeBlockVertical,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      );
                    },

                    //Unavailable Days Builder
                    unavailableDayBuilder: (context, date, _) => Column(
                      children: [
                        Opacity(opacity: 0.0),
                      ],
                    ),

                    //outside WeekendDay Builder
                    outsideWeekendDayBuilder: (context, date, _) => Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 1 * SizeConfig.safeBlockVertical),
                      child: Column(
                        children: [
                          Expanded(
                            child: Opacity(
                              opacity: 0.6,
                              child: Text(
                                date.day.toString(),
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 3.5 * SizeConfig.safeBlockVertical,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Opacity(
                              opacity: 0.6,
                              child: Text(
                                HijriCalendar.fromDate(date).hDay.toString(),
                                style: TextStyle(
                                  color: Colors.red[400],
                                  fontSize: 3.5 * SizeConfig.safeBlockVertical,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    //OutSide Day Builder
                    outsideDayBuilder: (context, date, _) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 1 * SizeConfig.safeBlockVertical),
                        child: Column(
                          children: [
                            Expanded(
                              child: Opacity(
                                opacity: 0.6,
                                child: Text(
                                  date.day.toString(),
                                  style: TextStyle(
                                    color: Colors.black54,
                                    fontSize:
                                        3.5 * SizeConfig.safeBlockVertical,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Opacity(
                                opacity: 0.6,
                                child: Text(
                                  HijriCalendar.fromDate(date).hDay.toString(),
                                  style: TextStyle(
                                    color: Colors.red[400],
                                    fontSize:
                                        3.5 * SizeConfig.safeBlockVertical,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },

                    //  Selected Day Builder
                    selectedDayBuilder: (context, date, events) => Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 0.5 * SizeConfig.safeBlockVertical),
                      child: Column(
                        children: [
                          Expanded(
                            child: Container(
                              alignment: Alignment.center,
                              height: 7.87 * SizeConfig.safeBlockVertical,
                              width: 7.87 * SizeConfig.safeBlockVertical,
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                shape: BoxShape.circle,
                                // borderRadius: BorderRadius.all(Radius.circular(20.0)),
                              ),
                              child: Text(
                                date.day.toString(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 3.5 * SizeConfig.safeBlockVertical,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                              child: Text(
                            HijriCalendar.fromDate(date).hDay.toString(),
                            style: TextStyle(
                                color: Colors.redAccent,
                                fontSize: 3.5 * SizeConfig.safeBlockVertical,
                                fontWeight: FontWeight.bold),
                          )),
                        ],
                      ),
                    ),
                  ),
                  onVisibleDaysChanged: _onVisibleDaysChanged,
                  onDaySelected: _onDaySelected,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _monthsList.map((month) {
                int index = _monthsList.indexOf(month);
                if (index != 0) {
                  return Container(
                    width: 1.75 * SizeConfig.safeBlockVertical,
                    height: 1.75 * SizeConfig.safeBlockVertical,
                    margin: EdgeInsets.symmetric(
                        vertical: 1.3 * SizeConfig.safeBlockVertical,
                        horizontal: 0.4 * SizeConfig.safeBlockVertical),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentSliderPos == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                } else {
                  return SizedBox();
                }
              }).toList(),
            ),
          ),
          SizedBox(
            height: 1 * SizeConfig.safeBlockVertical,
          ),
        ],
      ),
    );
  }
}
