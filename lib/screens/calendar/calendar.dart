import 'package:flutter/material.dart';
import 'package:prayer/screens/calendar/show_calendar.dart';
import 'package:prayer/shared/size_config.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 10 * SizeConfig.safeBlockVertical,
        backgroundColor: Color(0xff2372AD),
        title: Text('Islamic Calendar',
        style: TextStyle(
          fontSize: 3.93 * SizeConfig.safeBlockVertical,
          fontWeight: FontWeight.bold,
        ),
        ),
        centerTitle: true,
      ),
      body: ShowCalendar(),
    );
  }
}
